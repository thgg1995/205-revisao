package br.com.itau.imersivo.carteiraInvestimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersivo.carteiraInvestimento.model.Aplicacao;
import br.com.itau.imersivo.carteiraInvestimento.service.AplicacaoService;

@RestController
public class AplicacaoController {

	@Autowired
	AplicacaoService aplicacaoService;

	@GetMapping("/cliente/{id}/aplicacao")
	public Iterable<Aplicacao> listarClientes(@PathVariable Long id) {
		return aplicacaoService.obterAplicacoesCliente(id);
	}

	@PostMapping("/cliente/{id}/aplicacao")
	public void criarAplicacao(@PathVariable Long id, @RequestBody Aplicacao aplicacao) {
		aplicacaoService.criarAplicacao(id, aplicacao);
	}

}
