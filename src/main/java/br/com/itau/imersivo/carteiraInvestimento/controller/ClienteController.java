package br.com.itau.imersivo.carteiraInvestimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersivo.carteiraInvestimento.model.Cliente;
import br.com.itau.imersivo.carteiraInvestimento.service.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	@GetMapping("/cliente")
	public Iterable<Cliente> listarClientes() {
		return clienteService.obterClientes();
	}

	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarCliente(@RequestBody Cliente cliente) {
		clienteService.criarCliente(cliente);
	}
	
	@PatchMapping("/cliente/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
}
