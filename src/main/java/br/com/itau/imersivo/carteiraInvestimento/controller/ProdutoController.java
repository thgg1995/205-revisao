package br.com.itau.imersivo.carteiraInvestimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersivo.carteiraInvestimento.model.Produto;
import br.com.itau.imersivo.carteiraInvestimento.service.ProdutoService;

@RestController

public class ProdutoController {

	@Autowired
	private ProdutoService produtoService;

	@GetMapping("/produto")
	public Iterable<Produto> listarProdutos() {
		return produtoService.obterProdutos();
	}

	@PostMapping("/produto")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarProduto(@RequestBody Produto produto) {
		produtoService.gravarProduto(produto);
	}

	@DeleteMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deletarProduto(@PathVariable Long id) {
		produtoService.deletarProduto(id);
	}

	@PutMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void atualizarProduto(@PathVariable Long id, @RequestBody Produto produto) {
		produtoService.editarProduto(id, produto);
	}

}
