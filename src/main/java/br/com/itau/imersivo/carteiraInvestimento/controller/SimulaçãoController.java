package br.com.itau.imersivo.carteiraInvestimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersivo.carteiraInvestimento.model.Produto;
import br.com.itau.imersivo.carteiraInvestimento.model.Simulacao;
import br.com.itau.imersivo.carteiraInvestimento.service.ProdutoService;

@RestController

public class SimulaçãoController {
	@Autowired
	private ProdutoService produtoService;

	@PostMapping("/simulacao")
	public Simulacao calcularSimulacao(@RequestBody Simulacao simulacao) {

		Produto produto = produtoService.obterProduto(simulacao.getProduto().getId());
		
		Simulacao resultado = new Simulacao();

		double valorFinal = Math.pow((produto.getRendimento() + 1), simulacao.getMeses()) * simulacao.getValor();
		
		resultado.setValorFinal(valorFinal);
		
		return resultado;
	}
}
