package br.com.itau.imersivo.carteiraInvestimento.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

public class Simulacao {
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private double valor;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private int meses;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private Produto produto;
	
	private double valorFinal;

	public double getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

	public int getMeses() {
		return meses;
	}

	public void setMeses(int meses) {
		this.meses = meses;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public double getValorFinal() {
		return valorFinal;
	}

	public void setValorFinal(double valorFinal) {
		this.valorFinal = valorFinal;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
