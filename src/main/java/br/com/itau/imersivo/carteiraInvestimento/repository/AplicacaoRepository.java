package br.com.itau.imersivo.carteiraInvestimento.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.imersivo.carteiraInvestimento.model.Aplicacao;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long> {

	Iterable<Aplicacao> findAllByCliente_Id(Long id);
}
