package br.com.itau.imersivo.carteiraInvestimento.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.imersivo.carteiraInvestimento.model.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long>{
	
}
