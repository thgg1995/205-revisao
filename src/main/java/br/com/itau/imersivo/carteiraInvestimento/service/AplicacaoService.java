package br.com.itau.imersivo.carteiraInvestimento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.imersivo.carteiraInvestimento.model.Aplicacao;
import br.com.itau.imersivo.carteiraInvestimento.model.Cliente;
import br.com.itau.imersivo.carteiraInvestimento.repository.AplicacaoRepository;

@Service
public class AplicacaoService {

	@Autowired
	AplicacaoRepository aplicacaoRepository;

	@Autowired
	ClienteService clienteService;

	@Autowired
	ProdutoService produtoService;

	public Iterable<Aplicacao> obterAplicacoesCliente(Long id) {
		return aplicacaoRepository.findAllByCliente_Id(id);
	}

	public void criarAplicacao(Long id, Aplicacao aplicacaoNova) {
		Cliente cliente = clienteService.obterCliente(id);

		produtoService.obterProduto(aplicacaoNova.getProduto().getId());
		aplicacaoNova.setCliente(cliente);
		aplicacaoNova.setProduto(aplicacaoNova.getProduto());

		aplicacaoRepository.save(aplicacaoNova);
	}
}
