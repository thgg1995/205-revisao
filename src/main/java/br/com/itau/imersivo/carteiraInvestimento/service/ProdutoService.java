package br.com.itau.imersivo.carteiraInvestimento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.imersivo.carteiraInvestimento.model.Produto;
import br.com.itau.imersivo.carteiraInvestimento.repository.ProdutoRepository;



@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos() {
		return produtoRepository.findAll();
	}
	
	public Produto obterProduto(Long id) {
		return encontraOuDaErro(id);
	}
	
	public void gravarProduto(Produto produto) {
		produtoRepository.save(produto);
	}
	
	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);
		
		produto.setNome(produtoAtualizado.getNome());
		produto.setRendimento(produtoAtualizado.getRendimento());
		
		return produtoRepository.save(produto);
	}
	
	public void deletarProduto(Long id) {
		Produto produto = encontraOuDaErro(id);
		
		produtoRepository.delete(produto);
		
	}
	
	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}
		
		return optional.get();
	}

}
