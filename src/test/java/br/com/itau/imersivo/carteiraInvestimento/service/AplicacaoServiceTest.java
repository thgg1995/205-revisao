package br.com.itau.imersivo.carteiraInvestimento.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.imersivo.carteiraInvestimento.model.Aplicacao;
import br.com.itau.imersivo.carteiraInvestimento.model.Produto;
import br.com.itau.imersivo.carteiraInvestimento.repository.AplicacaoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { AplicacaoService.class })
public class AplicacaoServiceTest {

	@Autowired
	AplicacaoService aplicacaoService;

	@MockBean
	AplicacaoRepository aplicacaoRepository;
	
	@MockBean
	ClienteService clienteService;
	
	@MockBean
	ProdutoService produtoService;

	@Test
	public void deveListarTodosAsAplicacoes() {
		Long id = 1L;
		Produto produto = new Produto();
		produto.setId(id);
		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setId(id);
		aplicacao.setMeses(24);
		aplicacao.setProduto(produto);
		aplicacao.setValor(10000);

		List<Aplicacao> aplicacoes = new ArrayList<>();
		aplicacoes.add(aplicacao);

		when(aplicacaoRepository.findAllByCliente_Id(id)).thenReturn(aplicacoes);

		List<Aplicacao> resultado = Lists.newArrayList(aplicacaoService.obterAplicacoesCliente(id));

		assertEquals(resultado.get(0).getId(), id);
		assertEquals(resultado.get(0).getMeses(), 24);
		assertEquals(resultado.get(0).getProduto().getId(), id);
	}

}
