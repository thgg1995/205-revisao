package br.com.itau.imersivo.carteiraInvestimento.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.imersivo.carteiraInvestimento.model.Cliente;
import br.com.itau.imersivo.carteiraInvestimento.repository.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ClienteService.class })
public class ClienteServiceTest {

	@Autowired
	ClienteService clienteService;

	@MockBean
	ClienteRepository clienteRepository;

	@Test
	public void deveListarTodosOsClientes() {

		List<Cliente> clientes = new ArrayList<>();
		clientes.add(new Cliente());

		when(clienteRepository.findAll()).thenReturn(clientes);

		Iterable<Cliente> resultado = clienteService.obterClientes();

		assertEquals(1, Lists.list(resultado).size());
	}

	@Test
	public void deveListarUmClientePorId() {
		Long id = 1L;
		String cpfNovo = "44894242700";
		
		Cliente cliente = new Cliente();
		cliente.setCpf(cpfNovo);
		cliente.setId(id);
		cliente.setNome("Thiago");

		when(clienteRepository.findById(id)).thenReturn(Optional.of(cliente));

		Cliente resultado = clienteService.obterCliente(id);

		assertEquals(resultado.getCpf(), cpfNovo);
		assertEquals(resultado.getId(), id);
		assertEquals(resultado.getNome(), "Thiago");
		
	}

	@Test
	public void deveCriarUmCliente() {
		Cliente cliente = new Cliente();

		clienteService.criarCliente(cliente);

		Mockito.verify(clienteRepository).save(cliente);
	}

	@Test
	public void deveEditarUmCliente() {
		long id = 1;
		String novoNome = "Thiago Ramalho";
		Cliente clienteDoBanco = new Cliente();
		Cliente clienteDoFront = new Cliente();

		clienteDoBanco.setId(1L);
		clienteDoFront.setId(2L);
		clienteDoFront.setNome(novoNome);

		when(clienteRepository.findById(id)).thenReturn(Optional.of(clienteDoBanco));
		when(clienteRepository.save(clienteDoBanco)).thenReturn(clienteDoBanco);

		Cliente resultado = clienteService.editarCliente(id, clienteDoFront);

		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
	}
}
