package br.com.itau.imersivo.carteiraInvestimento.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.imersivo.carteiraInvestimento.model.Produto;
import br.com.itau.imersivo.carteiraInvestimento.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ProdutoService.class })
public class ProdutoServiceTest {

	@Autowired
	ProdutoService produtoService;

	@MockBean
	ProdutoRepository ProdutoRepository;

	@Test
	public void deveListarTodosOsProdutos() {

		List<Produto> produtos = new ArrayList<>();
		produtos.add(new Produto());

		when(ProdutoRepository.findAll()).thenReturn(produtos);

		Iterable<Produto> resultado = produtoService.obterProdutos();

		assertNotNull(resultado);
	}

	@Test
	public void deveListarUmProdutoPorId() {
		Long id = 1L;
		Double rendimento = 0.009;
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("CDB");
		produto.setRendimento(rendimento);

		when(ProdutoRepository.findById(id)).thenReturn(Optional.of(produto));

		Produto resultado = produtoService.obterProduto(id);

		assertEquals(resultado.getId(), id);
		assertEquals(resultado.getNome(), "CDB");
		assertEquals(resultado.getRendimento(), rendimento);
	}

	@Test
	public void deveCriarUmProduto() {
		Produto produto = new Produto();

		produtoService.gravarProduto(produto);

		Mockito.verify(ProdutoRepository).save(produto);
	}
	
	@Test
	public void deveEditarUmProduto() {
		long id = 1;
		String novoNome = "Fundo";
		Produto produtoDoBanco = new Produto();
		Produto produtoDoFront = new Produto();

		produtoDoBanco.setId(1L);
		produtoDoFront.setId(2L);
		produtoDoFront.setNome(novoNome);

		when(ProdutoRepository.findById(id)).thenReturn(Optional.of(produtoDoBanco));
		when(ProdutoRepository.save(produtoDoBanco)).thenReturn(produtoDoBanco);

		Produto resultado = produtoService.editarProduto(id, produtoDoFront);

		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
	}
}
